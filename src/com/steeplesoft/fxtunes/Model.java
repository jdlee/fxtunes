/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.fxtunes;

import entagged.audioformats.AudioFile;
import entagged.audioformats.AudioFileIO;
import entagged.audioformats.Tag;
import entagged.audioformats.exceptions.CannotReadException;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author jdlee
 */
public class Model {

    private Connection connection;
    public ObservableList artists = FXCollections.observableArrayList();
    public ObservableList albums = FXCollections.observableArrayList();
    public ObservableList songs = FXCollections.observableArrayList();
    public String status;
    private boolean changesFound = false;

    public Model() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:fxtunes.db");
            try (Statement stmt = connection.createStatement()) {
                stmt.execute("CREATE TABLE IF NOT EXISTS foo (int bar);");
                stmt.execute("CREATE TABLE IF NOT EXISTS songs (id int primary key, title varchar, album varchar, artist varchar, path varchar)");
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(FxTunes.class.getName()).log(Level.SEVERE, null, ex);
        }

        loadArtists();
    }

    protected final void loadArtists() {
        List<String> list = new ArrayList<>();

        try (Statement stmt = connection.createStatement(); ResultSet rs = stmt.executeQuery("SELECT DISTINCT artist FROM songs ORDER BY artist")) {
            while (rs.next()) {
                list.add(rs.getString(1));
            }
            artists.clear();
            artists.addAll(list);
        } catch (SQLException ex) {
            Logger.getLogger(MetadataThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void recordSong(String artist, String album, String title, String path) {
        try (PreparedStatement stmt = connection.prepareStatement("INSERT INTO songs (artist, album, title, path) values (?, ?, ?, ?)")) {
            stmt.setString(1, artist);
            stmt.setString(2, album);
            stmt.setString(3, title);
            stmt.setString(4, path);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MetadataThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isSongInDatabase(String path) {
        boolean exists = false;
        try (PreparedStatement stmt = connection.prepareStatement("SELECT COUNT(*) FROM songs WHERE path = ?")) {
            stmt.setString(1, path);
            ResultSet rs = stmt.executeQuery();
            exists = rs.getInt(1) > 0;
        } catch (SQLException ex) {
            Logger.getLogger(MetadataThread.class.getName()).log(Level.SEVERE, null, ex);
        }

        return exists;
    }

    void loadAlbums(String artist) {
        albums.clear();
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT DISTINCT album FROM songs WHERE artist = ? ORDER BY album");
            stmt.setString(1, artist);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                albums.add(new Album(artist, rs.getString(1)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MetadataThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void loadSongs(String artist, String album) {
        List<String> list = new ArrayList<>();

        try {
            PreparedStatement stmt =
                    connection.prepareStatement("SELECT DISTINCT title FROM songs WHERE artist = ? AND album = ? ORDER BY artist, album, title");
            stmt.setString(1, artist);
            stmt.setString(2, album);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                list.add(rs.getString(1));
            }
            songs.clear();
            songs.addAll(list);
        } catch (SQLException ex) {
            Logger.getLogger(MetadataThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getPath(String artist, String album, String title) {
        String path = null;
        try (PreparedStatement stmt = connection.prepareStatement("SELECT path FROM songs WHERE artist = ? AND album = ? AND title = ?")) {
            stmt.setString(1, artist);
            stmt.setString(1, album);
            stmt.setString(1, title);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                path = rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MetadataThread.class.getName()).log(Level.SEVERE, null, ex);
        }

        return path;
    }

    public void validateMusicDir() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                checkForMissingSongs();
                processDir("/Users/jdlee/Music");
                if (changesFound) {
                    loadArtists();
                    changesFound = false;
                }
            }
        });
    }

    private void checkForMissingSongs() {
        try (PreparedStatement stmt = connection.prepareStatement("SELECT path FROM songs ORDER BY artist, album");
                PreparedStatement delete = connection.prepareStatement("DELETE FROM songs WHERE PATH = ?");) {
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                String path = rs.getString(1);
                if (!new File(path).exists()) {
                    delete.setString(1, path);
                    delete.execute();
                    changesFound = true;
                }
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(MetadataThread.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void processDir(String dir) {
        File file = new File(dir);
        File[] files = file.listFiles(new Mp3FileFilter());

        for (File f : files) {
            final String path = f.getAbsolutePath();
            if (f.isDirectory()) {
                processDir(path);
            } else {
                if (!isSongInDatabase(path)) {
                    try {
                        changesFound = true;
                        status = ("Processing " + path);
                        AudioFile af = AudioFileIO.read(f);
                        Tag tag = af.getTag();
                        String artist = tag.getFirstArtist();
                        String album = tag.getFirstAlbum();
                        String title = tag.getFirstTitle();
                        recordSong(artist, album, title, path);
                    } catch (CannotReadException ex) {
                        Logger.getLogger(FxTunes.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        try {
            Thread.sleep(50);
        } catch (InterruptedException ex) {
        }
    }
}
