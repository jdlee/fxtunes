/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.fxtunes;

import java.io.File;
import java.io.FileFilter;

/**
 *
 * @author jdlee
 */
class Mp3FileFilter implements FileFilter {
    @Override
    public boolean accept(File pathname) {
        return pathname.isDirectory() || pathname.getName().endsWith(".mp3");
    }
    
}
