/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.fxtunes.subsonic;

import java.net.URI;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author jdlee
 */
public class SubsonicClient {

    private String url;
    private String user;
    private String pass;
    private String version = "1.10.1";
    private Client client;
    private Map<String, List<Album>> albumsByArtist = new HashMap<>();
    private long indexesModifiedSince;
    final private List<Index> indexes = new ArrayList<>();
    final private List<Album> albums = new ArrayList<>();

    public SubsonicClient(String url, String user, String password) {
        try {
            this.url = url;
            this.user = user;
            this.pass = password;

            SSLContext ctx = SSLContext.getInstance("SSL");
            ctx.init(new KeyManager[0], new TrustManager[]{new DefaultTrustManager()}, new SecureRandom());
            SSLContext.setDefault(ctx);
            client = ClientBuilder.newBuilder()
                    .hostnameVerifier(new NoopHostnameVerifier())
                    .sslContext(ctx)
                    .build();
        } catch (NoSuchAlgorithmException | KeyManagementException ex) {
            Logger.getLogger(SubsonicClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Index> getIndexes() {
        if (indexes.isEmpty()) {
            readIndexes(buildRequestUri("getIndexes")
                    .queryParam("ifModifiedSince", indexesModifiedSince)
                    .request()
                    .get(String.class));
        }
        return indexes;
    }

    public List<Artist> getArtists() {
        List<Artist> artists = new ArrayList<>();
        for (Index index : getIndexes()) {
            artists.addAll(index.getArtists());
        }

        return artists;
    }

    public List<Album> getAlbums(Artist artist) {
        return getAlbums(artist.getId());
    }

    public List<Album> getAlbums(final String artistId) {
        List<Album> albums = new ArrayList<>();
        String json = buildRequestUri("getMusicDirectory")
                .queryParam("id", artistId)
                .request()
                .get(String.class);
        try {
            JSONObject response = new JSONObject(json).getJSONObject("subsonic-response");
            if ("ok".equals(response.getString("status"))) {
                JSONObject dirObj = response.optJSONObject("directory");
                if (dirObj != null) {
                    JSONArray childArray = dirObj.optJSONArray("child");
                    if (childArray != null) {
                        for (int i = 0; i < childArray.length(); i++) {
                            JSONObject child = childArray.getJSONObject(i);
                            Album album = getAlbumFromJson(child);
                            albums.add(album);
                        }
                    } else {
                        JSONObject childObj = dirObj.optJSONObject("child");
                        if (childObj != null) {
                            albums.add(getAlbumFromJson(childObj));
                        }
                    }
                }
            }
        } catch (JSONException ex) {
            Logger.getLogger(SubsonicClient.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }

        return albums;
    }

    public List<Song> getAlbum(Album album) {
        return getAlbum(album.getId());
    }

    public List<Song> getAlbum(String albumId) {
        List<Song> songs = new ArrayList<>();
        String json = buildRequestUri("getMusicDirectory")
                .queryParam("id", albumId)
                .request()
                .get(String.class);
        try {
            JSONObject response = new JSONObject(json).getJSONObject("subsonic-response");
            if ("ok".equals(response.getString("status"))) {
                JSONObject dirObj = response.optJSONObject("directory");
                if (dirObj != null) {
                    JSONArray childArray = dirObj.optJSONArray("child");
                    if (childArray != null) {
                        for (int i = 0; i < childArray.length(); i++) {
                            JSONObject child = childArray.getJSONObject(i);
                            Song song = new Song();
                            song.setId(child.getString("id"));
                            song.setTitle(child.getString("title"));
                            song.setGenre(child.optString("genre"));
                            song.setAlbumId(child.optString("albumId"));
                            song.setTrack(child.optInt("track"));
                            song.setParentId(child.optString("parent"));
                            song.setDuration(child.optInt("duration"));
                            song.setArtist(child.optString("artist"));
                            song.setArtistId(child.optString("artistId"));
                            song.setPath(child.optString("path"));
                            song.setYear(child.optInt("year"));
                            song.setBitRate(child.optInt("bitrate"));
                            song.setCoverArt(child.optString("coverArt"));
                            songs.add(song);
                        }
                    } else {
                        throw new RuntimeException("eep!");
                    }
                }
            }
        } catch (JSONException ex) {
            Logger.getLogger(SubsonicClient.class.getName()).log(Level.SEVERE, null, ex);
        }

        return songs;
    }

    public URI getSongUri(Song song) {
        WebTarget wt = buildRequestUri("stream")
                .queryParam("id", song.getId())
                .queryParam("format", "mp3");
        byte[] bytes = wt.request().get(byte[].class);
        System.out.println(bytes.length);
        return wt.getUri();
    }

    public Map<String, Playlist> getPlaylists(String owner) {
        Map<String, Playlist> playlists = new HashMap<>();
        try {
            String json = buildRequestUri("getPlaylists")
                    //                    .queryParam("id", artistId)
                    .request().get(String.class);
            JSONObject response = new JSONObject(json).getJSONObject("subsonic-response");
            if ("ok".equals(response.getString("status"))) {
                JSONArray array = response.getJSONObject("playlists").optJSONArray("playlist");
                if (array != null) {
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        Playlist playlist = new Playlist();
                        playlist.setId(obj.getString("id"));
                        playlist.setDuration(obj.optLong("duration"));
                        playlist.setSongCount(obj.optLong("songCount"));
                        playlist.setName(obj.getString("name"));
                        playlist.setOwner(obj.optString("owner"));
                        playlist.setPublic(obj.optBoolean("public"));
                        playlist.setComment(obj.optString("comment"));
                        playlists.put(playlist.getName(), playlist);
                    }
                }
            }
        } catch (JSONException ex) {
            Logger.getLogger(SubsonicClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return playlists;
    }

    /**
     * ***********************************************************************
     */
    private WebTarget buildRequestUri(String endpoint) {
        return client.target(url + "/rest/" + endpoint + ".view")// +queryParams;
                .queryParam("u", user)
                .queryParam("p", pass)
                .queryParam("v", version)
                .queryParam("c", "subsonicClient")
                .queryParam("f", "json");
    }

    private Album getAlbumFromJson(JSONObject child) throws JSONException {
        Album album = new Album();
        album.setArtistId(child.optString("parent"));
        album.setArtistName(child.optString("artist"));
        album.setCoverArt(child.optString("coverArt"));
        album.setId(child.getString("id"));
        String title = child.optString("title");
        if (title != null && title.startsWith("[")) {
            int index = title.indexOf("]");

            try {
                Integer year = Integer.parseInt(title.substring(1, index)); // TODO: Bounds checking
                album.setYear(year.toString());
            } catch (NumberFormatException nfe) {
            }

            title = title.substring(index + 1).trim();
        }
        album.setTitle(title);
        return album;
    }

    private static class ArtistComparator implements Comparator<Artist> {

        @Override
        public int compare(Artist o1, Artist o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }

    private void readIndexes(String json) {
        try {
            JSONObject response = new JSONObject(json).getJSONObject("subsonic-response");
            if ("ok".equals(response.getString("status"))) {
                JSONObject indexesObj = response.optJSONObject("indexes");
                if (indexesObj != null) {
                    JSONArray indexArray = indexesObj.optJSONArray("index");
                    if (indexArray != null) {
                        for (int i = 0; i < indexArray.length(); i++) {
                            final JSONObject idxObj = indexArray.getJSONObject(i);
                            Index index = new Index();
                            index.setName(idxObj.optString("name"));
                            JSONArray artistArray = idxObj.optJSONArray("artist");
                            List<Artist> artists = new ArrayList<>();
                            if (artistArray != null) {
                                for (int j = 0; j < artistArray.length(); j++) {
                                    JSONObject obj = artistArray.getJSONObject(j);
                                    Artist artist = new Artist();
                                    artist.setId(obj.getString("id"));
                                    artist.setName(obj.getString("name"));
                                    artists.add(artist);
                                }
                            }
                            index.setArtists(artists);
                            indexes.add(index);
                        }
                    }
                    indexesModifiedSince = indexesObj.getLong("lastModified");
                }
            } else {
                //
            }
        } catch (JSONException ex) {
            Logger.getLogger(SubsonicClient.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private List<Artist> readIndexes1(String json) {
        List<Artist> artists = new ArrayList<>();
        try {
            JSONObject response = new JSONObject(json).getJSONObject("subsonic-response");
            if ("ok".equals(response.getString("status"))) {
                JSONObject indexes = response.optJSONObject("indexes");
                if (indexes != null) {
                    JSONArray index = indexes.optJSONArray("index");
                    if (index != null) {
                        for (int i = 0; i < index.length(); i++) {
                            JSONArray artistArray = index.getJSONObject(i).optJSONArray("artist");
                            if (artistArray != null) {
                                for (int j = 0; j < artistArray.length(); j++) {
                                    JSONObject obj = artistArray.getJSONObject(j);
                                    Artist artist = new Artist();
                                    artist.setId(obj.getString("id"));
                                    artist.setName(obj.getString("name"));
                                    artists.add(artist);
                                }
                            }
                        }
                    }
                }
            } else {
                //
            }
        } catch (JSONException ex) {
            Logger.getLogger(SubsonicClient.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }

        Collections.sort(artists, new ArtistComparator());
        return artists;

    }

    private List<Artist> readArtists(String json) {
        List<Artist> artists = new ArrayList<>();
        try {
            JSONObject response = new JSONObject(json).getJSONObject("subsonic-response");
            if ("ok".equals(response.getString("status"))) {
                JSONArray array = response.getJSONObject("artists").getJSONArray("index");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject o = (JSONObject) array.get(i);
                    JSONArray artistArray = o.optJSONArray("artist");
                    if (artistArray != null) {
                        for (int j = 0; j < artistArray.length(); j++) {
                            JSONObject artistObj = (JSONObject) artistArray.get(j);
                            Artist artist = buildArtist(artistObj);
                            artists.add(artist);
                        }
                    } else {
                        JSONObject artistObj = o.optJSONObject("artist");
                        if (artistObj != null) {
                            Artist artist = buildArtist(artistObj);
                            artists.add(artist);
                        }
                    }
                }
            }
        } catch (JSONException ex) {
            Logger.getLogger(SubsonicClient.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }

        Collections.sort(artists, new ArtistComparator());
        return artists;
    }

    private Artist buildArtist(JSONObject artistObj) throws JSONException {
        Artist artist = new Artist();
        artist.setId(artistObj.getString("id"));
        artist.setName(artistObj.getString("name"));
        artist.setGenre(artistObj.optString("genre"));
        artist.setAlbumCount(artistObj.optString("albumCount"));
        return artist;
    }

    private static class DefaultTrustManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }

    private static class NoopHostnameVerifier implements HostnameVerifier {

        @Override
        public boolean verify(String string, SSLSession ssls) {
            return true;
        }
    };

    private List<Album> getAlbumsOld(Artist artist) {
        List<Album> filtered = new ArrayList<>();
        synchronized (albums) {
            if (albums.isEmpty()) {
                try {
                    boolean foundAll = false;
                    int offset = 0;
                    while (!foundAll) {
                        int read = 0;
                        final WebTarget req = buildRequestUri("getAlbumList")
                                .queryParam("size", "500")
                                .queryParam("type", "alphabeticalByName")
                                .queryParam("offset", offset);
                        String json = req
                                .request()
                                .get(String.class);
                        JSONObject response = new JSONObject(json).getJSONObject("subsonic-response");
                        if ("ok".equals(response.getString("status"))) {
                            JSONObject albumList = response.optJSONObject("albumList");
                            if (albumList != null) {
                                JSONArray children = albumList.optJSONArray("album");
                                if (children != null) {
                                    for (int i = 0; i < children.length(); i++) {
                                        JSONObject child = children.getJSONObject(i);
                                        albums.add(new Album(child.getString("id"), child.getString("title"),
                                                child.optString("parent"), child.optString("artist"),
                                                child.optString("coverArt")));
                                        read++;
                                    }
                                } else {
                                    JSONObject child = response.optJSONObject("album");
                                    if (child != null) {
                                        albums.add(new Album(child.getString("id"), child.getString("title"),
                                                child.optString("parent"), child.getString("artist"),
                                                child.optString("coverArt")));
                                        read++;
                                    }
                                }
                            } else {
                                foundAll = true;
                            }

                            offset += read;
                        }
                    }
                } catch (JSONException ex) {
                    Logger.getLogger(SubsonicClient.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        if (artist != null) {
            for (Album album : albums) {
                if (album.getArtistName().equals(artist.getName())) {
                    filtered.add(album);
                }
            }
        } else {
            filtered = albums;
        }
        return filtered;
    }
}
