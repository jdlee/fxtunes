/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.fxtunes.subsonic;

/**
 *
 * @author jdlee
 */
public class Playlist {
    private String id;
    private Long duration;
    private Long songCount;
    private String name;
    private String owner;
    private boolean publicList;
    private String comment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getSongCount() {
        return songCount;
    }

    public void setSongCount(Long songCount) {
        this.songCount = songCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public boolean isPublic() {
        return publicList;
    }

    public void setPublic(boolean publicList) {
        this.publicList = publicList;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "Playlist{" + "id=" + id + ", duration=" + duration + ", songCount=" + songCount + ", name=" + name + ", owner=" + owner + ", publicList=" + publicList + ", comment=" + comment + '}';
    }
}
