/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.fxtunes.subsonic;

/**
 *
 * @author jdlee
 */
public class Artist {
    private String id;
    private String name;
    private String genre;
    private String coverArt;
    private String albumCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getCoverArt() {
        return coverArt;
    }

    public void setCoverArt(String coverArt) {
        this.coverArt = coverArt;
    }

    public String getAlbumCount() {
        return albumCount;
    }

    public void setAlbumCount(String albumCount) {
        this.albumCount = albumCount;
    }

    @Override
    public String toString() {
        return "Artist{" + "id=" + id + ", name=" + name + ", genre=" + genre + ", coverArt=" + coverArt + ", albumCount=" + albumCount + '}';
    }
    
    
}
