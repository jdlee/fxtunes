/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.fxtunes.subsonic;

/**
 *
 * @author jdlee
 */
public class Album {
    private String id;
    private String title;
    private String artistId;
    private String artistName;
    private String year;
    private String coverArt;

    public Album() {
    }

    public Album(String id, String title, String artistId, String artistName, String coverArt) {
        this.id = id;
        this.title = title;
        this.artistId = artistId;
        this.artistName = artistName;
        this.coverArt = coverArt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCoverArt() {
        return coverArt;
    }

    public void setCoverArt(String coverArt) {
        this.coverArt = coverArt;
    }

    @Override
    public String toString() {
        return "Album{" + "id=" + id + ", title=" + title + ", artistId=" + artistId + ", artistName=" + artistName + ", coverArt=" + coverArt + '}';
    }
}
