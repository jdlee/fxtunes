/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.fxtunes;

import com.steeplesoft.fxtunes.cells.ArtistCell;
import com.steeplesoft.fxtunes.cells.SongCell;
import com.steeplesoft.fxtunes.cells.AlbumCell;
import com.steeplesoft.fxtunes.subsonic.Artist;
import com.steeplesoft.fxtunes.subsonic.Song;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Callback;
import javafx.util.Duration;

/**
 *
 * @author jdlee
 */
public class FXTunesController implements Initializable {

    private Image pauseImg;
    private Image playImg;
    private ImageView playPauseIcon;
    private SubsonicModel model;
    private MediaPlayer mediaPlayer;

    @FXML
    ListView artistList;
    @FXML
    ListView albumList;
    @FXML
    ListView songsList;
    @FXML
    Button btn_playPause;
    @FXML
    Slider positionSlider;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        model = new SubsonicModel("creed", "4040", "test", "!test1234");
        initializeArtistList();
        initializeAlbumList();
        initializeSongsList();

        URL imgUrl = getClass().getResource("resources/pause.png");
        pauseImg = new Image(imgUrl.toString());

        imgUrl = getClass().getResource("resources/play.png");
        playImg = new Image(imgUrl.toString());

        playPauseIcon = new ImageView(playImg);
        btn_playPause.setGraphic(playPauseIcon);

        positionSlider.valueChangingProperty().addListener(new PositionListener());

        model.loadArtists();

    }

    private void initializeSongsList() {
        songsList.setItems(model.songs);
        songsList.setCellFactory(new Callback<ListView<Song>, ListCell<Song>>() {
            @Override
            public ListCell<Song> call(ListView<Song> p) {
                return new SongCell();
            }
        });
    }

    private void initializeAlbumList() {
        albumList.setItems(model.albums);
        albumList.setCellFactory(new Callback<ListView<com.steeplesoft.fxtunes.subsonic.Album>, ListCell<com.steeplesoft.fxtunes.subsonic.Album>>() {
            @Override
            public ListCell<com.steeplesoft.fxtunes.subsonic.Album> call(ListView<com.steeplesoft.fxtunes.subsonic.Album> p) {
                return new AlbumCell();
            }
        });
    }

    private void initializeArtistList() {
        artistList.setItems(model.artists);
        artistList.setCellFactory(new Callback<ListView<Artist>, ListCell<Artist>>() {
            @Override
            public ListCell<Artist> call(ListView<Artist> p) {
                return new ArtistCell();
            }
        });
    }

    @FXML
    public void handleArtistClick(MouseEvent t) {
        model.loadAlbums((Artist) artistList.getSelectionModel().getSelectedItem());
        albumList.getSelectionModel().clearSelection();
        model.songs.clear();
    }

    @FXML
    public void handleAlbumClick(MouseEvent t) {
        com.steeplesoft.fxtunes.subsonic.Album album = (com.steeplesoft.fxtunes.subsonic.Album) albumList.getSelectionModel().getSelectedItem();
        if (album != null) {
            model.loadSongs(album, album.getArtistName(), album.getTitle());
        }
    }

    @FXML
    public void handleSongClick(MouseEvent t) {
        if (t.getClickCount() == 2) {
            Media media = new Media(model.getSongUrl((Song) songsList.getSelectionModel().getSelectedItem()).toString());
            if (mediaPlayer != null) {
                mediaPlayer.stop();
            }
            mediaPlayer = new MediaPlayer(media);
            mediaPlayer.play();
        }

    }

    @FXML
    public void handlePlayPauseClick(ActionEvent e) {

    }

    @FXML
    public void handlePrevClick(ActionEvent e) {

    }

    @FXML
    public void handleNextClick(ActionEvent e) {

    }

    private class PositionListener implements ChangeListener<Boolean> {

        @Override
        public void changed(ObservableValue<? extends Boolean> observable,
                Boolean oldValue, Boolean newValue) {
            if (oldValue && !newValue) {
                double pos = positionSlider.getValue();
//                final MediaPlayer mediaPlayer = songModel.getMediaPlayer();
//                final Duration seekTo = mediaPlayer.getTotalDuration().multiply(pos);
//                seekAndUpdatePosition(seekTo);
            }
        }
    }
}
