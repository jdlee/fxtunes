/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.fxtunes;

import com.steeplesoft.fxtunes.subsonic.Album;
import com.steeplesoft.fxtunes.subsonic.Artist;
import com.steeplesoft.fxtunes.subsonic.Song;
import com.steeplesoft.fxtunes.subsonic.SubsonicClient;
import java.net.URI;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author jdlee
 */
public class SubsonicModel {
    private final SubsonicClient client;
    private String baseUrl = "http://creed:4040";
    private String host;
    private String port;
    private String user = "test";
    private String password = "!test1234";
    public String status;
    public ObservableList artists = FXCollections.observableArrayList();
    public ObservableList albums = FXCollections.observableArrayList();
    public ObservableList songs = FXCollections.observableArrayList();

    public SubsonicModel(String host, String port, String user, String password) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;
        baseUrl = "http://" + host + ":"  + port;
        client = new SubsonicClient(baseUrl, user, password);
    }

    void loadArtists() {
        artists.clear();
        artists.addAll(client.getArtists());
    }

    void loadAlbums(Artist artist) {
        albums.clear();
        final List<Album> albums1 = client.getAlbums(artist);
        albums.addAll(albums1);
    }

    void loadSongs(Album album, String artist, String title) {
        songs.clear();
        songs.addAll(client.getAlbum(album));
    }

    public URI getSongUrl(Song song) {
        return client.getSongUri(song);
    }
}
