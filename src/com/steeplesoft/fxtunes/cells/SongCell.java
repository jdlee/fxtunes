/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.fxtunes.cells;

import com.steeplesoft.fxtunes.subsonic.Song;
import javafx.scene.control.ListCell;

/**
 *
 * @author jdlee
 */
public class SongCell extends ListCell<Song> {

    @Override
    public void updateItem(Song item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            setText(item.getTitle());
        }
    }

}
