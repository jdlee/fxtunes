/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.fxtunes.cells;

import com.steeplesoft.fxtunes.subsonic.Artist;
import javafx.scene.control.ListCell;

/**
 *
 * @author jdlee
 */
public class ArtistCell extends ListCell<Artist> {

    @Override
    public void updateItem(Artist item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            setText(item.getName());
        }
    }

}
