/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.fxtunes.cells;

import javafx.scene.control.ListCell;

/**
 *
 * @author jdlee
 */
public class AlbumCell extends ListCell<com.steeplesoft.fxtunes.subsonic.Album> {

    @Override
    public void updateItem(com.steeplesoft.fxtunes.subsonic.Album item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            setText(item.getTitle());
        }
    }

}
