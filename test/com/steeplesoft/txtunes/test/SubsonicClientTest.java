/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.txtunes.test;

import com.steeplesoft.fxtunes.subsonic.Album;
import com.steeplesoft.fxtunes.subsonic.Artist;
import com.steeplesoft.fxtunes.subsonic.Index;
import com.steeplesoft.fxtunes.subsonic.Playlist;
import com.steeplesoft.fxtunes.subsonic.Song;
import com.steeplesoft.fxtunes.subsonic.SubsonicClient;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import junit.framework.Assert;
import net.subclient.subsonic.SubsonicConnection;
import net.subclient.subsonic.mappings.IndexInfo;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author jdlee
 */
public class SubsonicClientTest {
    private SubsonicClient client;

    @Before
    public void setup() {
        client = new SubsonicClient("http://creed:4040", "test", "!test1234");
    }

    @Test
    public void testIndexes() {
        List<Index> indexes = client.getIndexes();
        Assert.assertNotNull(indexes);
        Assert.assertFalse(indexes.isEmpty());
    }

    @Test
    public void testArtists() {
        List<Artist> artists = client.getArtists();
        Assert.assertNotNull(artists);
        Assert.assertFalse(artists.isEmpty());
    }

    @Test
    public void testAlbums() {
        List<Artist> artists = client.getArtists();
        for (Artist artist : artists) {
            List<Album> albums = client.getAlbums(artist);
            Assert.assertNotNull(albums);
        }
    }

    @Test
    public void testGetAlbum() {
        Artist artist = client.getArtists().get(2);
        for (Album album : client.getAlbums(artist)) {
            List<Song> songs = client.getAlbum(album);
            Assert.assertNotNull(songs);
        }
    }

    @Test
    public void testPlaylists() {
        Map<String,Playlist> playlists = client.getPlaylists(null);
        Assert.assertNotNull(playlists);
        Assert.assertFalse(playlists.isEmpty());
    }

//    @Test
    public void testSubsonicConnection() {
        try {
            System.out.println("******************** testSubsonicConnection");
            SubsonicConnection conn = new SubsonicConnection(new URL("https://creed:1443/"),
                    "admin", "bosco2", "myApp", false);
            final ArrayList<IndexInfo> indexesArray = conn.getIndexes().getIndexes().getIndexesArray();
            System.out.println(indexesArray.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("******************** testSubsonicConnection");
    }
}
